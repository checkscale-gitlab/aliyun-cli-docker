#!/usr/bin/env bash

if [ -z "${DOCKER_HUB_REPOSITORY}" ]; then
  echo 'Set the "DOCKER_HUB_REPOSITORY" environment variable first. e.g. "<my dockerhub account>/<my docker image>"'
  exit 1
fi

function login_to_docker_hub()
{
  if [ -n "${DOCKER_HUB_PASSWORD}" ]; then
    if [ "${is_docker_hub_authenticated}" != "true" ]; then
      echo "Logging into Docker Hub..."
      echo ${DOCKER_HUB_PASSWORD} | docker login -u ${DOCKER_HUB_USERNAME} --password-stdin 2> /dev/null
      is_docker_hub_authenticated=true
    fi
  else
    echo "Skipping docker login as DOCKER_HUB_PASSWORD has not been set..."
  fi
}

function get_build_architectures()
{
  local docker_files=($(ls | grep .Dockerfile))

  local architectures=""
  for docker_file in ${docker_files[@]}; do
    architectures+=" ${docker_file/.Dockerfile/}"
  done

  echo ${architectures}
}

# Return the latest 20 versions on GitHub (versions older than v3.0.26 have build issues)
function get_versions_from_github()
{
  echo $(wget -q "https://api.github.com/repos/aliyun/aliyun-cli/tags?page=1&per_page=20" -O - | jq -r ".[].name")
}

function get_versions_from_docker_hub()
{
  echo $(wget -q "https://registry.hub.docker.com/v1/repositories/${DOCKER_HUB_REPOSITORY}/tags" -O - | jq -r ".[].name")
}

function build_image()
{
  architecture=${1}
  aliyun_cli_version=${2}

  echo "Building: ${architecture}:${aliyun_cli_version}"

  # Deterministic variables
  image_label="${DOCKER_HUB_REPOSITORY}:${aliyun_cli_version}"
  architecture_image_label="${image_label}-${architecture}"

  # Pull the image if it already exists
  docker pull "${architecture_image_label}" || true

  # Build the image
  docker build \
    --cache-from "${architecture_image_label}" \
    --build-arg ALIYUN_CLI_VERSION=${aliyun_cli_version} \
    -t "${architecture_image_label}" \
    -f "${architecture}.Dockerfile" \
    .

  # Push the image
  if [ "${is_docker_hub_authenticated}" = "true" ]; then
    docker push "${architecture_image_label}"
  fi
}

function build_manifest()
{
  aliyun_cli_version=${1}

  echo "Building Manifest: ${aliyun_cli_version}"

  # Create the list of architecture images to be linked to the manifest
  build_architectures=($(get_build_architectures))

  # Deterministic variables
  image_label="${DOCKER_HUB_REPOSITORY}:${aliyun_cli_version}"
  architecture_image_label="${image_label}-${architecture}"

  architecture_image_labels=""
  for build_architecture in ${build_architectures[@]}; do
    architecture_image_labels+=" ${image_label}-${build_architecture}"
  done

  # Create and push the manifest file
  docker manifest create "${image_label}" ${architecture_image_labels}
  if [ "${is_docker_hub_authenticated}" = "true" ]; then
    docker manifest push --purge "${image_label}"
  fi
}

function set_as_latest_version()
{
  aliyun_cli_version=${1}

  image_label="${DOCKER_HUB_REPOSITORY}:${aliyun_cli_version}"
  latest_image_label="${DOCKER_HUB_REPOSITORY}:latest"

  current_image_digest=$(docker manifest inspect ${image_label} | jq .manifests[0].digest)
  current_latest_image_digest=$(docker manifest inspect ${latest_image_label} | jq .manifests[0].digest)

  # No need to create the 'latest' manifest
  # if the 'aliyun_cli_version' already matches 'latest'
  if [ "${current_image_digest}" = "${current_latest_image_digest}" ]; then
    echo "SKIP: ${image_label} is already set as 'latest'"
    return
  fi

  echo "Setting 'latest' Manifest: ${image_label}"

  # Create the list of architecture images to be linked to the manifest
  build_architectures=($(get_build_architectures))

  architecture_image_labels=""
  for build_architecture in ${build_architectures[@]}; do
    architecture_image_labels+=" ${image_label}-${build_architecture}"
  done

  # Create and push the manifest file
  docker manifest create "${latest_image_label}" ${architecture_image_labels}
  if [ "${is_docker_hub_authenticated}" = "true" ]; then
    docker manifest push --purge "${latest_image_label}"
  fi
}

function trigger_build_jobs()
{
  echo -n "Triggering Build Jobs..."

  if [ -z "${GITLAB_TRIGGER_TOKEN}" ]; then
    echo "skipped!"
    echo "Please set the GITLAB_TRIGGER_TOKEN, GITLAB_TRIGGER_REF_NAME and GITLAB_TRIGGER_API_ENDPOINT environment variables."
    return
  fi

  curl -X POST -F token=${GITLAB_TRIGGER_TOKEN} -F ref=${GITLAB_TRIGGER_REF_NAME} ${GITLAB_TRIGGER_API_ENDPOINT}
  echo "done."
}

# Defaults
architecture=${1:-amd64}
is_docker_hub_authenticated=false

# Login to Docker Hub
login_to_docker_hub
