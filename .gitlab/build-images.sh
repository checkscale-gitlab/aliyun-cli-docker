#!/usr/bin/env bash

# Stop on any non-zero return codes.
set -e

# Get the current location of this script
CURRENT_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# Import shared functions
source "${CURRENT_DIR}/common.sh"

github_versions=($(get_versions_from_github))
docker_hub_versions=$(get_versions_from_docker_hub)

# Loop through the github versions
for github_version in ${github_versions[@]}; do
  # Build the versions that are not on docker hub
  (
    docker_hub_version=${github_version/v/}
    docker_hub_architecture_version="${docker_hub_version}-${architecture}"
    echo ${docker_hub_versions} | grep -q ${docker_hub_architecture_version} || build_image "${architecture}" "${docker_hub_version}"
  )
done
